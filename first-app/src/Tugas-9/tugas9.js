import React, { Component } from "react";

class Tugas9 extends Component {
  render() {
    return (
      <div className="App">
        <h1>Form Pembelian Buadh</h1>
        <form>
          <div>
            <label for="name" className="nameTxt">
              Nama Pelanggan
            </label>
            <input type="text" />
          </div>
          <div className="orderContainer">
            <div className="left-half">
              <label for="order" className="boldTxt">
                Daftar Item
              </label>
            </div>
            <div className="right-half">
              <div>
                <input type="checkbox" value="fruit1" value="Semangka" />{" "}
                <label for="fruit1">Semangka</label>
              </div>
              <div>
                <input type="checkbox" value="fruit2" value="Jeruk" />{" "}
                <label for="fruit2">Jeruk</label>
              </div>
              <div>
                <input type="checkbox" value="fruit3" value="Nanas" />{" "}
                <label for="fruit3">Nanas</label>
              </div>
              <div>
                <input type="checkbox" value="fruit4" value="Salak" />{" "}
                <label for="fruit4">Salak</label>
              </div>
              <div>
                <input type="checkbox" value="fruit5" value="Anggur" />{" "}
                <label for="fruit5">Anggur</label>
              </div>
            </div>
          </div>
          <input type="submit" value="Kirim" className="submitBtn" />
        </form>
      </div>
    );
  }
}

export default Tugas9;
