import React, { useState } from "react";
import logo from "./logo.svg";
import "./App.css";
import Tugas9 from "../src/Tugas-9/tugas9";
import Tugas10 from "../src/Tugas-10/tugas10";
import Tugas11 from "../src/Tugas-11/tugas11";

import Tugas12 from "../src/Tugas-12/tugas12";
import Tugas13 from "../src/Tugas-13/tugas13";

import Tugas14 from "../src/Tugas-14/DaftarBuah";
import Tugas15 from "../src/Tugas-15/tugas15";
import {BrowserRouter as Router} from "react-router-dom";

import ThemeContext, { themes } from '../src/Tugas-15/theme-context'; 

function App() {
  const [theme, setTheme] = useState(themes.dark);

  const toggleTheme = () => 
  theme === themes.dark ? setTheme(themes.light) : setTheme(themes.dark);
  return (
   <div>
      <ThemeContext.Provider value={theme}>
        <button onClick={toggleTheme}>Change Theme</button>
      <Router>
      <Tugas15/>
      </Router>
      </ThemeContext.Provider>
      </div>
  );
}

export default App;