import React, { Component } from "react";

class Tugas12 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataHargaBuah: [
        { nama: "Semangka", harga: 10000, berat: 1000 },
        { nama: "Anggur", harga: 40000, berat: 500 },
        { nama: "Strawberry", harga: 30000, berat: 400 },
        { nama: "Jeruk", harga: 30000, berat: 1000 },
        { nama: "Mangga", harga: 30000, berat: 500 },
      ],
      nama: "",
      harga: null,
      berat: null,
      edited: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.addData = this.addData.bind(this);
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
    console.log([event.target.name]);
  }

  addData(event) {
    event.preventDefault();
    const tempData = [...this.state.dataHargaBuah];
    const newData = {
      nama: this.state.nama,
      harga: this.state.harga,
      berat: this.state.berat,
    };

    this.setState({
      dataHargaBuah: tempData,
      nama: "",
      harga: null,
      berat: null,
    });
    tempData.push(newData);
  }

  editData = (index, event) => {
    const temp = [...this.state.dataHargaBuah];
    temp[index].nama = <input type="text" name="nama" />;
    temp[index].harga = <input type="number" min="0" name="harga" />;
    temp[index].berat = <input type="number" min="0" name="berat" />;
    temp.splice(index, 0, temp);
    this.setState({ temp: temp });
  };

  deleteData = (index, event) => {
    const dataHargaBuah = ([], this.state.dataHargaBuah);
    dataHargaBuah.splice(index, 1);
    this.setState({ dataHargaBuah: dataHargaBuah });
  };

  render() {
    return (
      <div className="App2">
        <h1>Tabel Harga Buah</h1>
        <table className="tableArea">
          <thead className="tableHeader">
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Harga</th>
              <th>Berat</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {this.state.dataHargaBuah.map((val, index) => {
              return (
                <tr className="tableData">
                  <td>{index + 1}</td>
                  <td>{val.nama}</td>
                  <td>{val.harga}</td>
                  <td>{(val.berat)/1000} kg</td>
                  <td>
                    <button onClick={this.editData.bind(this, index)} style={{width: "50%", color: "black", backgroundColor: "yellow"}}>
                      Edit Data
                    </button>
                    <button onClick={this.deleteData.bind(this, index)} style={{width: "50%", color: "black", backgroundColor: "red"}}>
                      Delete Data
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        {/* Form */}
        <h1>Form Pemesanan Buah</h1>
        <form>
            <div>
          <label for="nama">Masukkan Nama Buah:</label>
          <input
            type="text"
            value={this.state.nama}
            onChange={this.handleChange}
            name="nama"
          />{" "}
          </div>
          <div>
          <label for="harga">Masukkan Harga Buah:</label>
          <input
            type="number" min="0"
            value={this.state.harga}
            onChange={this.handleChange}
            name="harga"
          />{" "}
          </div>
          <div>
          <label for="nama">Masukkan Berat Buah (gr):</label>
          <input
            type="number" min="0"
            value={this.state.berat}
            onChange={this.handleChange}
            name="berat"
          />{" "}
          </div>
          <button onClick={this.addData}>Add Data</button>
        </form>
      </div>
    );
  }
}

export default Tugas12;
