import React, { Component } from "react";

let dataHargaBuah = [
  { nama: "Semangka", harga: 10000, berat: 1000 },
  { nama: "Anggur", harga: 40000, berat: 500 },
  { nama: "Strawberry", harga: 30000, berat: 400 },
  { nama: "Jeruk", harga: 30000, berat: 1000 },
  { nama: "Mangga", harga: 30000, berat: 500 },
];

class Tugas10 extends Component {
  render() {
    return (
      <div className="App2">
        <h1>Tabel Harga Buah</h1>
          <table className="tableArea">
            <tr className="tableHeader">
              <th>Nama</th>
              <th>Harga</th>
              <th>Berat</th>
            </tr>
            {dataHargaBuah.map(el => {
                return(
                    <tr className="tableData">
                        <td>{el.nama}</td>
                        <td>{el.harga}</td>
                        <td>{el.berat/1000} kg</td>
                    </tr>
                )
            })}
          </table>
      </div>
    );
  }
}

export default Tugas10;
