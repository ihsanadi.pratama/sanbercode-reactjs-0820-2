import React, { Component } from "react";

class Tugas11 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      time: new Date().toLocaleTimeString(),
      seconds: 110,
      show: true,
    };
  }

  componentDidMount() {
    if (this.props.start !== undefined) {
      this.setState({ time: this.props.start });
    }
    this.timerID = setInterval(() => this.tick(), 1000);

    this.myInterval = setInterval(() => this.countdown(), 1000);
  }

  countdown() {
    if (this.state.seconds === 0) {
      clearInterval(this.myInterval);
      this.setState({
        show: false,
      });
    } else {
      this.setState({
        seconds: this.state.seconds - 1,
      });
    }
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
    clearInterval(this.myInterval);
  }

  tick() {
    this.setState({
      time: new Date().toLocaleTimeString(),
    });
  }
  render() {
    return (
      <>
        {this.state.show ? (
          <div className="timeline">
            <h1 className="divider">sekarang jam: {this.state.time}</h1>
            <h1 className="divider">hitung mundur: {this.state.seconds}</h1>
          </div>
        ) : null}
      </>
    );
  }
}

export default Tugas11;