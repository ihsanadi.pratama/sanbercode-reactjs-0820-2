import React, { useContext } from "react";
import { Switch, Route } from "react-router";

import Tugas9 from "../Tugas-9/tugas9";
import Tugas10 from "../Tugas-10/tugas10";
import Tugas11 from "../Tugas-11/tugas11";

import Tugas12 from "../Tugas-12/tugas12";
import Tugas13 from "../Tugas-13/tugas13";

import Tugas14 from "../Tugas-14/DaftarBuah";
import Nav from "../Tugas-15/Nav";

import ThemeContext from './theme-context';

const Tugas15 = () => {
    const theme = useContext(ThemeContext);
  return (
      <div style={theme}>
    <Switch>
      <Route exact path="/">
        <Nav />
      </Route>
      <Route exact path="/tugas9">
        <Tugas9 />
      </Route>
      <Route path="/tugas10">
        <Tugas10 />
      </Route>
      <Route exact path="/tugas11">
        <Tugas11 />
      </Route>
      <Route exact path="/tugas12">
        <Tugas12 />
      </Route>
      <Route exact path="/tugas13">
        <Tugas13 />
      </Route>
      <Route exact path="/tugas14">
        <Tugas14 />
      </Route>
    </Switch>
    </div>
  );
};

export default Tugas15;

