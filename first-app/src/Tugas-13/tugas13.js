import React, { useState, useEffect } from "react";
import axios from "axios";

const Tugas13 = () => {
  const [dataHargaBuah, setDataHargaBuah] = useState(null);
  const [input, setInput] = useState({ nama: "", harga: "", berat: 0 });
  const [edited, setEdited] = useState(-1);

  const handleChange = (event) => {
    let typeOfInput = event.target.name;
    switch (typeOfInput) {
      case "nama": {
        setInput(event.target.value);
        break;
      }
      case "harga": {
        setInput(event.target.value);
        break;
      }
      case "berat": {
        setInput(event.target.value);
        break;
      }
      default: {
        break;
      }
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    let name = input.nama;
    let price = input.harga;
    let weight = input.berat;

    if (name.replace(/\s/g, "") !== "" && price.replace(/\s/g, "") !== "") {
      let newDataHargaBuah = dataHargaBuah;
      let index = edited;

      if (index === -1) {
        newDataHargaBuah = [...newDataHargaBuah, { name, price, weight }];
      } else {
        newDataHargaBuah[index].nama = name;
        newDataHargaBuah[index].harga = price;
        newDataHargaBuah[index].berat = weight;
      }
      setDataHargaBuah(newDataHargaBuah);
      setInput({ nama: "", harga: "", berat: 0 });
    }
  };

  const editData = (event) => {
    let index = parseInt(event.target.value);
    let hargaBuah = dataHargaBuah.find((x) => x.id === dataHargaBuah);
    setInput(...input, {
      nama: hargaBuah.nama,
      harga: hargaBuah.harga,
      berat: hargaBuah.berat,
    });
  };

  const deleteData = (event) => {
    let index = parseInt(event.target.value);
    axios
      .delete(`http://backendexample.sanbercloud.com/api/fruits/${index}`)
      .then((res) => {
        var newDataHargaBuah = dataHargaBuah.filter((x) => x.id !== index);
        setDataHargaBuah([...newDataHargaBuah]);
        console.log();
      });
    let newDataHargaBuah = dataHargaBuah;
    let editedDataBuah = newDataHargaBuah[edited];
    newDataHargaBuah.splice(index, 1);

    if (editedDataBuah !== undefined) {
      var newIndex = newDataHargaBuah.findIndex((el) => el === editedDataBuah);
      setDataHargaBuah([...newDataHargaBuah]);
      setEdited(newIndex);
    } else {
      setDataHargaBuah([...newDataHargaBuah]);
    }
  };

  useEffect(() => {
    axios
      .get(`http://backendexample.sanbercloud.com/api/fruits`)
      .then((res) => {
        setDataHargaBuah(res.data);
        console.log(res.data);
      });
    axios
      .post(`http://backendexample.sanbercloud.com/api/fruits`)
      .then((res) => {});
    axios
      .put(`http://backendexample.sanbercloud.com/api/fruits/{ID_FRUIT}`)
      .then((res) => {});
  }, [dataHargaBuah]);

  return (
    <div className="App2">
      <h1>Tabel Harga Buah</h1>
      <table className="tableArea">
        <thead className="tableHeader">
          <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Harga</th>
            <th>Berat</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {dataHargaBuah !== null &&
            dataHargaBuah.map((val, index) => {
              return (
                <tr className="tableData">
                  <td>{index + 1}</td>
                  <td>{val.nama}</td>
                  <td>{val.harga}</td>
                  <td>{val.berat / 1000} kg</td>
                  <td>
                    <button
                      onClick={editData}
                      value={index}
                      style={{
                        width: "50%",
                        color: "black",
                        backgroundColor: "yellow",
                      }}
                    >
                      Edit Data
                    </button>
                    <button
                      onClick={deleteData}
                      value={index}
                      style={{
                        width: "50%",
                        color: "black",
                        backgroundColor: "red",
                      }}
                    >
                      Delete Data
                    </button>
                  </td>
                </tr>
              );
            })}
        </tbody>
      </table>
      {/* Form */}
      <h1>Form Pemesanan Buah</h1>
      <form onSubmit={handleSubmit}>
        <div>
          <label for="nama">Masukkan Nama Buah:</label>
          <input
            type="text"
            value={input.nama}
            onChange={handleChange}
            name="nama"
          />{" "}
        </div>
        <div>
          <label for="harga">Masukkan Harga Buah:</label>
          <input
            type="text"
            min="0"
            value={input.harga}
            onChange={handleChange}
            name="harga"
          />{" "}
        </div>
        <div>
          <label for="nama">Masukkan Berat Buah (gr):</label>
          <input
            type="number"
            min="0"
            value={input.berat}
            onChange={handleChange}
            name="berat"
          />{" "}
        </div>
        <button>Submit</button>
      </form>
    </div>
  );
};

export default Tugas13;
