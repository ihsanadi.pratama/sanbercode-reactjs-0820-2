import React, {useContext, useEffect} from "react"
import axios from "axios"
import {DaftarBuahContext} from "./DaftarBuahContext"


const DaftarBuahList = () =>{

  const [daftarBuah, setDaftarBuah] = useContext(DaftarBuahContext)

  useEffect( () => {
    if (daftarBuah.lists === null){
      axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
      .then(res => {
        setDaftarBuah({
          ...daftarBuah, 
          lists: res.data.map(el=>{ 
            return {id: el.id,
              name: el.name, 
              price: el.price, 
              weight: el.weight 
            }
          })
        })
      })
    }
  }, [setDaftarBuah, daftarBuah])

  const editData  = (event) =>{
    let idDataBuah = parseInt(event.target.value)
    setDaftarBuah({...daftarBuah, selectedId: idDataBuah, statusForm: "changeToEdit"})
  }

  const deleteData  = (event) => {
    let idDataBuah = parseInt(event.target.value)

    let newLists = daftarBuah.lists.filter(el => el.id !== idDataBuah)

    axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idDataBuah}`)
    .then(res => {
      console.log(res)
    })
          
    setDaftarBuah({...daftarBuah, lists: [...newLists]})
    
  }

  return(
    <div className="App2">
      <h1>Daftar Harga Buah</h1>
      <table className="tableArea">
        <thead className="tableHeader">
          <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Harga</th>
            <th>Berat</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>

            {
              daftarBuah.lists !== null && daftarBuah.lists.map((val, index)=>{
                return(                    
                  <tr key={index} className="tableData">
                    <td>{index+1}</td>
                    <td>{val.name}</td>
                    <td>{val.price}</td>
                    <td>{val.weight/1000} Kg</td>
                    <td>
                      <button onClick={editData} value={val.id} style={{
                        width: "50%",
                        color: "black",
                        backgroundColor: "yellow",
                      }}>Edit</button>
                      &nbsp;
                      <button onClick={deleteData} value={val.id} style={{
                        width: "50%",
                        color: "black",
                        backgroundColor: "red",
                      }}>Delete</button>
                    </td>
                  </tr>
                )
              })
            }
        </tbody>
      </table>      
    </div>
  )
}

export default DaftarBuahList
